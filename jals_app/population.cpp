#include "population.h"

#include <random>
#include <iostream>
#include <algorithm>

Population::Population()
{

}

void Population::init(int count, b2World &world)
{
    this->initGeneration(count, world);
}

void Population::stepIn()
{
//    if (this->step > 1000) {
//        this->step = 0;
//        this->nextGeneration();
//    }
    for (auto &&agent : this->agents) {
        agent->step();
    }
//    this -> step += 1;
}

int Population::size()
{
    return static_cast<int>(this->agents.size());
}

void Population::initGeneration(int count, b2World &world)
{
    for (int i = 0; i < count; ++i) {
        Agent::Position pos = Agent::generatePositionForIth(i, count);
        std::unique_ptr<Agent> a = std::make_unique<Agent>(pos.x, pos.y, world);
        this->agents.push_back(std::move(a));
    }
}

void Population::initGeneration(int count, b2World &world, std::vector<Gene> gene_pool)
{
    std::random_device rd;
    std::mt19937 engine(rd());
    std::uniform_int_distribution<size_t> dist(0, gene_pool.size() - 1);

    for (int i = 0; i < count; ++i) {
        Agent::Position pos = Agent::generatePositionForIth(i, count);
        Gene  specimen1 = gene_pool[dist(engine)],
              specimen2 = gene_pool[dist(engine)];
        std::unique_ptr<Agent> a = std::make_unique<Agent>(pos.x, pos.y, world,
                                            Gene::recombine(specimen1, specimen2));
        this->agents.push_back(std::move(a));
    }
}

void Population::nextGeneration(int count, b2World &world)
{
    std::sort(this->agents.begin(), this->agents.end(),
              [](const std::unique_ptr<Agent> &a, const std::unique_ptr<Agent> &b) {
        return a->energy > b->energy;
    });
    std::vector<Gene> top_gene_agents;
    for (unsigned int i = 0; i < 100; i++) {
        top_gene_agents.push_back(this->agents[i]->gene);
    }
    this->agents.clear();
    this->initGeneration(count, world, top_gene_agents);
}

std::vector<ObjectVisualData> Population::prepareData()
{
    std::vector<ObjectVisualData> data;
    for (auto &&agent : this->agents) {
        data.push_back(agent->getNewRenderData());
    }
    return data;
}

Agent* Population::getAgentByXY (int x, int y, b2World &world) const
{
    BodyQueryCallback callback;
    b2AABB aabb;
    aabb.lowerBound.Set(float(x)-2.0f, float(y)-2.0f);
    aabb.upperBound.Set(float(x)+2.0f, float(y)+2.0f);
    world.QueryAABB(&callback, aabb);
    Agent* agent = nullptr;
    if (callback.fb) {
        agent = callback.agent;
    }
    return agent;
}
