#include "agent.h"

#include <iostream>

const float Agent::DEFAULTRADIUS = 2.0;

Agent::Position Agent::generatePositionForIth(int i, int from)
{
   float rad = 200.0f;
   float angle = float(360.0F / from * i);
   return Agent::Position(rad * cos(angle), rad * sin(angle));
}

Agent::Agent(float x, float y, b2World &world) :
            gene(Gene::getRandom()),
            fenotype(gene),
            radius(Agent::DEFAULTRADIUS)
{
    this->initBody(x, y, world);
}

Agent::Agent(float x, float y, b2World &world, Gene preset_gene) :
            gene(preset_gene),
            fenotype(gene),
            radius(Agent::DEFAULTRADIUS)
{
    this->initBody(x, y, world);
}

Agent::~Agent()
{
    this->body->GetWorld()->DestroyBody( this->body );
}

void Agent::step()
{
    b2Vec2 pos = this->body->GetPosition();
    Eigen::Vector4f sensors;
    float center_val = Environment::getAt(pos.x, pos.y);
    sensors << (center_val - Environment::getAt(pos.x + radius, pos.y)),
               (center_val - Environment::getAt(pos.x - radius, pos.y)),
               (center_val - Environment::getAt(pos.x, pos.y + radius)),
               (center_val - Environment::getAt(pos.x, pos.y - radius));

    Eigen::Vector2f output;
    output = this->fenotype.data * sensors;
    b2Vec2 actuators(output[0], output[1]);
    this->body->ApplyForceToCenter(100.0f * actuators, true);

    this->energy += 0.002f * center_val;
}

Agent::Position Agent::getPosition() const
{
    b2Vec2 body_pos = this->body->GetPosition();
    Position position;
    position.x = body_pos.x;
    position.y = body_pos.y;
    return position;
}

ObjectVisualData Agent::getNewRenderData()
{
    b2Vec2 pos = this->body->GetPosition();
    this->visualData.x = pos.x;
    this->visualData.y = pos.y;
    this->visualData.hue = this->energy * 100;
    return this->visualData;
}

std::vector<int> Agent::getShape() const
{
    std::vector<int> s;
    s.push_back(int(this->fenotype.data.rows()));
    s.push_back(int(this->fenotype.data.cols()));
    return s;
}

std::vector<float> Agent::getGene() const
{
    Gene::GeneData matrix = this->gene.data;
    std::vector<float> vec (&matrix(0,0), matrix.data()+matrix.cols() * matrix.rows());
//    a.data() = this->fenotype.data.data()
    return vec;
}


std::vector<float> Agent::getFeno() const
{
    Fenotype::FenoData matrix = this->fenotype.data;
    std::vector<float> vec (&matrix(0,0), matrix.data()+matrix.cols() * matrix.rows());
//    a.data() = this->fenotype.data.data()
    return vec;
}

void Agent::initBody(float x, float y, b2World &world)
{
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.active = true;
    bodyDef.position.Set(x, y);
    this->body = world.CreateBody(&bodyDef);

    b2CircleShape circleShape;
    circleShape.m_p.Set(0, 0); //position, relative to body position
    circleShape.m_radius = this->radius; //radius

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circleShape; //this is a pointer to the shape above
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.3f;
    this->body->CreateFixture(&fixtureDef); //add a fixture to the body
    body->SetLinearDamping(0.01f);
    body->ResetMassData();

    body->SetUserData(this);

    this->visualData = {x, y, this->radius, 0};
}
