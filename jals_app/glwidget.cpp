
#include "glwidget.h"

#include <QPainter>
#include <QPaintEvent>
#include <QTimer>
#include <iostream>

GLWidget::GLWidget( QWidget *parent)
    : QOpenGLWidget(parent)
{
    this->elapsed = 0;
    this->setFixedSize(800, 800);
    this->setAutoFillBackground(false);

    QRadialGradient bg_gradient(QPointF(400, 400), 400.0);
    bg_gradient.setColorAt(1.0, Qt::white);
    bg_gradient.setColorAt(0.0, QColor(0xa6, 0xce, 0x39));
    this->background = QBrush(bg_gradient);

    this->circleBrush = QBrush(QColor(0xa6, 0xce, 0x39));
    this->circlePen = QPen(Qt::black);
    this->circlePen.setWidth(1);
}

void GLWidget::animate()
{
    this->elapsed = (this->elapsed + qobject_cast<QTimer*>(sender())->interval());
    this->update();
}

void GLWidget::initNewDataRequest()
{
    emit this->renderDataRequest(float((this->elapsed - this->last_elapsed_ms) * 0.001));
}

void GLWidget::recieveNewRenderData(const SimulationData &render_data)
{
    this->simData = render_data;
}

void GLWidget::displayAgentData(const AgentData &d)
{
    this->agentData = d;
    this->update();
}

void GLWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.fillRect(event->rect(), this->background);

    this->paintField(painter);
    this->paintAgent(painter);


    this->last_elapsed_ms = elapsed;
    painter.end();
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    // TODO: remove magic 400
    int x = event->x()-400, y = event->y()-400;
    this->emit requestAgentData(x, y);
}

void GLWidget::paintField(QPainter &painter)
{
    painter.translate(400, 400); // TODO: remove magic 400

    painter.save();
    painter.setBrush(this->circleBrush);
    painter.setPen(this->circlePen);


    for (auto object :  this->simData.getObjectsData()) {
        double diameter = 2 * double(object.radius);
        if (object.hue != 0.0f) {
            int hue = object.hue < 255 ? (object.hue > 0 ? int(object.hue) : 0): 255;
            painter.setBrush(QBrush(QColor::fromHsv(255 - hue, 200, 200)));
        }
        painter.drawEllipse(QRectF(double(object.x), double(object.y), diameter, diameter));
    }
    painter.drawText(QPoint(-380,-380),
                     "time: " + QString::number(this->last_elapsed_ms / 1000.0));
    if (this->simData.avgFitness() > this->max_energy) {
        this->max_energy = this->simData.avgFitness();
    }
    painter.drawText(QPoint(-380,-360),
                     "average fitness: " + QString::number(this->max_energy));
    painter.restore();
}

void GLWidget::paintAgent(QPainter &painter)
{
    if (!this->agentData.isEmpty()) {
        painter.save();
        int feno_m = this->agentData.getShape()[0], feno_n = this->agentData.getShape()[1];
        painter.drawText(QPoint(300, -380),
                         QString::number(feno_m) + "x" + QString::number(feno_n));
        AgentData::FenoVisType feno = this->agentData.getFeno();
        assert(feno_m * feno_n == feno.size());
        int i = 0, j = 0;
        for (auto data : feno) {
            QString out;
            out.sprintf("% 0.2f", double(data));
            painter.drawText(QPoint(300 + i*30, -360 + j*20), out);
            i++;
            if (i == feno_m ) {
                i = 0;
                j++;
            }
        }
        {
            QVector2D pos = agentData.getPosition();
            QString out;
            out.sprintf("Position = \n %0.2f, %0.2f ", double(pos.x()), double(pos.y()));
            painter.drawText(QPoint(280, -270), out);
        }
        painter.restore();
    }
}
