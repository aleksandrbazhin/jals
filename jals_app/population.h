#ifndef POPULATION_H
#define POPULATION_H

#include <QVector>
#include <Box2D/Box2D.h>
#include <vector>
#include <memory>
#include "agent.h"
#include "objectvisualdata.h"
#include "environment.h"


class Population
{
public:
    Population();
    void init(int count, b2World &world);
    void stepIn();
    int size();
    void nextGeneration(int count, b2World &world);
    int generation = 0;
    std::vector<ObjectVisualData> prepareData();
    Agent* getAgentByXY (int x, int y, b2World &world) const;
private:
    std::vector<std::unique_ptr<Agent>> agents;
    void initGeneration(int count, b2World &world);
    void initGeneration(int count, b2World &world,
                        std::vector<Gene> gene_pool);


//    Environment *environment;

};

#endif // POPULATION_H
