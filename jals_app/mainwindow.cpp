#include "mainwindow.h"

#include <QGridLayout>
#include <QLabel>
#include <QTimer>
#include <QPainter>
#include <iostream>

MainWindow::MainWindow()
{
    setWindowTitle(tr("JALS"));

    this->openGLwidget = new GLWidget(this);
//    QLabel *openGLLabel = new QLabel(tr("The life"));
//    openGLLabel->setAlignment(Qt::AlignHCenter);

    this->pauseButton = new QPushButton("Pause", this);
    // set size and location of the button

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(openGLwidget, 0, 1);

    QVBoxLayout *v_layout = new QVBoxLayout;
    v_layout->addWidget(this->pauseButton, 0, Qt::AlignTop);

    layout->addLayout(v_layout, 0, 2);
//    layout->addWidget(openGLLabel, 1, 1);
    this->setLayout(layout);

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, openGLwidget, &GLWidget::animate);
    timer->start(20);


    Simulation* simulation = new Simulation; // simulation worker
    simulation->moveToThread(&this->simulationThread);
    connect(&this->simulationThread, &QThread::finished, simulation, &QObject::deleteLater);
    qRegisterMetaType<SimulationData>("SimulationData");
    connect(simulation, &Simulation::renderDataReady, openGLwidget, &GLWidget::recieveNewRenderData);
    connect(this->pauseButton, &QPushButton::pressed, this, &MainWindow::togglePause);
    connect(this, &MainWindow::pauseSim, simulation, &Simulation::pause);
    connect(this, &MainWindow::unPauseSim, simulation, &Simulation::unPause);

    connect(openGLwidget, &GLWidget::requestAgentData, simulation, &Simulation::requestAgentDataByXY);
    qRegisterMetaType<AgentData>("AgentData");
    connect(simulation, &Simulation::agentDataReady, openGLwidget, &GLWidget::displayAgentData);

    this->simulationThread.start();

    QMetaObject::invokeMethod(simulation, "initWorld", Qt::QueuedConnection);
    QMetaObject::invokeMethod(simulation, "run", Qt::QueuedConnection);
    QMetaObject::invokeMethod(simulation, "setRenderOutput", Qt::QueuedConnection,
                              Q_ARG(bool, true), Q_ARG(qint64, 10));
}

MainWindow::~MainWindow()
{
    this->simulationThread.requestInterruption();
    this->simulationThread.quit();
    this->simulationThread.wait();
}

void MainWindow::togglePause()
{
    if (!this->simPaused) {
        emit this->pauseSim();
        this->simPaused = true;
        this->pauseButton->setText("Resume");
    } else {
        emit this->unPauseSim();
        this->simPaused = false;
        this->pauseButton->setText("Pause");
    }
}
