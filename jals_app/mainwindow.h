#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "simulation.h"
#include "glwidget.h"

#include <QWidget>
#include <QThread>
#include <QPushButton>

class MainWindow : public QWidget
{
    Q_OBJECT
    QThread simulationThread;

public:
    MainWindow();
    ~MainWindow() override;

signals:
    void pauseSim();
    void unPauseSim();

private:
    GLWidget *openGLwidget;
    QPushButton *pauseButton;
    bool simPaused = false;
private slots:
    void togglePause();
};

#endif

