#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <QBrush>
#include <QFont>
#include <QPen>
#include "objectvisualdata.h"
#include "simulation.h"

class GLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    GLWidget(QWidget *parent);

signals:
    void renderDataRequest(float);
    void requestAgentData(int, int);

public slots:
    void animate();
    void initNewDataRequest();
    void recieveNewRenderData(const SimulationData &render_data);
    void displayAgentData(const AgentData &d);

protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;

private:
    void paintField(QPainter &painter);
    void paintAgent(QPainter &painter);
    AgentData agentData;
    SimulationData simData;
    QBrush background;
    QBrush circleBrush;
    QPen circlePen;
    int elapsed;
    int last_elapsed_ms = 0;
    double max_energy = 0.0;
};

#endif
