QT       += core gui widgets

TARGET = jals_app
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        mainwindow.cpp \
    glwidget.cpp \
    simulation.cpp \
    agent.cpp \
    population.cpp \
    environment.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Box2D/release/ -lBox2D
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Box2D/debug/ -lBox2D
else:unix: LIBS += -L$$OUT_PWD/../../Box2D/ -lBox2D


INCLUDEPATH += $$PWD/../../Box2D
DEPENDPATH += $$PWD/../../Box2D
#QMAKE_CXXFLAGS += -isystem $$PWD/../../eigen # supress library warnings for eigen
QMAKE_CXXFLAGS += -Wno-attributes # the true workaround from https://github.com/openscad/openscad/commit/bd93f0acc4215223803b5c59f49935a9c4802828
INCLUDEPATH += $$PWD/../../eigen

HEADERS  += mainwindow.h \
    glwidget.h \
    simulation.h \
    agent.h \
    population.h \
    objectvisualdata.h \
    environment.h

CONFIG(debug, debug|release):copydll.commands = $(COPY_FILE) $$shell_path($$OUT_PWD/../../Box2D/debug/Box2D.dll) $$shell_path($$OUT_PWD)
CONFIG(release, debug|release):copydll.commands = $(COPY_FILE) $$shell_path($$OUT_PWD/../../Box2D/release/Box2D.dll) $$shell_path($$OUT_PWD)
first.depends = $(first) copydll
export(first.depends)
export(copydll.commands)
QMAKE_EXTRA_TARGETS += first copydll
