#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

class Environment
{
public:
    Environment();
    static float getAt(float x, float y);
};

#endif // ENVIRONMENT_H
