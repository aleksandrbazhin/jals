#include "simulation.h"

#include <QThread>
#include <QCoreApplication>

#include <iostream>

const int Simulation::VELOCITYITERATIONS = 6;
const int Simulation::POSITIONITERATIONS = 2;
const float Simulation::SIMTIMESTEP = 0.03f;
const float Simulation::REALTIMESTEP = 0.01f;
const int Simulation::POPCOUNT = 200;
const int Simulation::POPSTEPS = 3000;
const bool Simulation::REALTIME = false;

Simulation::Simulation() :
    realTime(Simulation::REALTIME),
    gravity(0.0f, 0.0f),
    world(gravity)
{

}

void Simulation::run()
{
    while (!QThread::currentThread()->isInterruptionRequested()) {
        // TODO: move this to population.cpp
        if (!this->paused) {
            if (this->popStep > Simulation::POPSTEPS) {
                this->popStep = 0;
                this->population.nextGeneration(
                            Simulation::POPCOUNT,
                            this->world);
            }
            this->population.stepIn();
            this->world.Step(Simulation::SIMTIMESTEP,
                             Simulation::VELOCITYITERATIONS,
                             Simulation::POSITIONITERATIONS);
            // environment.update()
            this->popStep++;
        }
        if (this->realTime) {
            QThread::msleep(uint32_t(Simulation::REALTIMESTEP * 1000));
        }
        qint64 elapsed = this->elapsedTimer.elapsed();
        if (this->isRendering &&
                elapsed - this->lastRenderDataSentMsec > renderDataTimeout) {
            this->lastRenderDataSentMsec = elapsed;
            this->sendRenderData();
        }
        QCoreApplication::processEvents();
    }
}

void Simulation::setRealTime(bool realTime)
{
    this->realTime = realTime;
}

void Simulation::setRenderOutput(bool isRendering, qint64 renderDataTimeout)
{
    this->isRendering = isRendering;
    this->renderDataTimeout = renderDataTimeout;
}

void Simulation::pause()
{
    this->paused = true;
}

void Simulation::unPause()
{
    this->paused = false;
}

void Simulation::requestAgentDataByXY(int x, int y)
{
    Agent* agent = this->population.getAgentByXY(x, y, this->world);
    if (agent) {
        emit this->agentDataReady(AgentData(*agent));
    }
}

void Simulation::initWorld()
{
    this->elapsedTimer.start();
    this->population.init(Simulation::POPCOUNT, this->world);
    // environment.create()
    // for item in environment0
    //      world.add (item.body)
}

void Simulation::sendRenderData()
{
    // renderdata += environment.getVisualData
    emit renderDataReady(SimulationData(this->population.prepareData())); // impicit copying by Qt when passing to another thread
}
