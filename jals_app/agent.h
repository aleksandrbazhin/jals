#ifndef AGENT_H
#define AGENT_H

#include <Box2D/Box2D.h>
#include <vector>
#include <iostream>
#include <memory>
#include "Eigen/Dense"
#include "objectvisualdata.h"
#include "environment.h"

class Gene {
public:
    typedef Eigen::Matrix<float, 2, 4> GeneData;
    static Gene recombine(const Gene &gene1, const Gene &gene2)
    {
        return Gene(gene1.data * 0.5f + gene2.data * 0.5f + 0.05f * GeneData::Random());
    }
    static GeneData getRandom() {
        return GeneData::Random();
    }
    Gene(GeneData init_data) : data(init_data) {}
    GeneData data;
};

class Fenotype {
public:
    typedef Eigen::Matrix<float, 2, 4> FenoData;
    Fenotype(const Gene &gene) : data(gene.data) {} //just copy for now
    FenoData data;
};

class Agent
{
public:
    typedef b2Vec2 Position;
    static const float DEFAULTRADIUS;
    static Position generatePositionForIth(int i, int from);
    Agent(float x, float y, b2World &world);
    Agent(float x, float y, b2World &world, Gene preset_gene);
    ~Agent();
    void step();
    Position getPosition() const;
    ObjectVisualData getNewRenderData();
    float energy = 0.0f;
    Gene gene;
    Fenotype fenotype;
    std::vector<int> getShape() const;
    std::vector<float> getGene() const;
    std::vector<float> getFeno() const;
    b2World* getWorld() const {return this->body->GetWorld();}
private:
    void initBody(float x, float y, b2World &world);
    ObjectVisualData visualData;
    float radius;
    b2Body* body;
};


class BodyQueryCallback : public b2QueryCallback
{
public:
    b2Body* foundBoby = nullptr;
    Agent* agent = nullptr;
    bool fb = false;
    bool ReportFixture(b2Fixture* fixture) {
        this->fb = true;
        this->foundBoby = fixture->GetBody();
        this->agent = static_cast<Agent*>(this->foundBoby->GetUserData());
        return false; // Return false to find only one
    }
};


#endif // AGENT_H
