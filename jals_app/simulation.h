#ifndef SIMULATION_H
#define SIMULATION_H

#include <QObject>
#include <Box2D/Box2D.h>
#include <QVector>
#include <QVector2D>
#include <QElapsedTimer>
#include "population.h"
#include "environment.h"
#include "objectvisualdata.h"

class SimulationData {
public:
    typedef QVector<ObjectVisualData> ObjectsData;
    SimulationData(){}
    SimulationData(std::vector<ObjectVisualData> o_data) :
        data(ObjectsData::fromStdVector(o_data)){}
    ObjectsData getObjectsData() {
        return this->data;
    }
    double avgFitness() {
        double sum = 0.0;
        for (auto obj: this->data) {
            sum += double(obj.hue);
        }
        return sum / this->data.size();
    }
private:
    ObjectsData data;
};

Q_DECLARE_METATYPE(SimulationData);

class AgentData {
public:
    typedef QVector<int> ShapVisType;
    typedef QVector<float> GeneVisType;
    typedef QVector<float> FenoVisType;

    AgentData(){}
    AgentData(const Agent& a) :
        empty(false),
        shape(ShapVisType::fromStdVector(a.getShape())),
        gene(GeneVisType::fromStdVector(a.getGene())),
        feno(FenoVisType::fromStdVector(a.getFeno())),
        position(a.getPosition().x, a.getPosition().y)
    {}
    bool isEmpty() {return this->empty;}
    ShapVisType getShape() const
    {
        return this->shape;
    }

    FenoVisType getFeno() const
    {
        return this->feno;
    }

    GeneVisType getGene() const
    {
        return this->gene;
    }

    QVector2D getPosition() const {
        return this->position;
    }
private:
    bool empty = true;
    ShapVisType shape;
    GeneVisType gene;
    FenoVisType feno;
    QVector2D position;
};

Q_DECLARE_METATYPE(AgentData);


class Simulation : public QObject
{
    Q_OBJECT

    static const int VELOCITYITERATIONS;
    static const int POSITIONITERATIONS;
    static const float SIMTIMESTEP;
    static const float REALTIMESTEP;
    static const int POPCOUNT;
    static const int POPSTEPS;
    static const bool REALTIME;

public:
    Simulation();
public slots:
    void initWorld();
    void run();
    void setRealTime(bool realTime);
    void setRenderOutput(bool isRendering, qint64 renderDataTimeout);
    void pause();
    void unPause();
    void requestAgentDataByXY(int x, int y);
signals:
    void renderDataReady(const SimulationData&);
    void agentDataReady(const AgentData&);
private:

    QElapsedTimer elapsedTimer;
    qint64 lastRenderDataSentMsec;
    qint64 renderDataTimeout = 30;
    bool isRendering = false;
    bool realTime;
    bool paused = false;
    Population population;
    Environment environment;
    b2Vec2 gravity;
    b2World world;
    int popStep = 0;
    void sendRenderData();

};

#endif // SIMULATION_H

